import { MessageBus } from './message-bus';
const messageBus = new MessageBus();
export const META_DATA_SUBSCRIBE = 'META_DATA_SUBSCRIBE';
export const META_DATA_SUBSCRIBE_EXPIRE_TIME  = 'META_DATA_SUBSCRIBE_EXPIRE_TIME';
export const META_DATA_SUBSCRIBE_CUSTOM = 'META_DATA_SUBSCRIBE_CUSTOM';
export const META_DATA_SUBSCRIBE_CUSTOM_PROVIDER =
  'META_DATA_SUBSCRIBE_CUSTOM_PROVIDER';

// export const EXCHANGE_DEFAULT = 'EXCHANGE_DEFAULT';
// export const QUEUE_TASK_ADD = 'HAHAHIHI';
// export const QUEUE_TASK_ADD = messageBus.buildNameQueue(
//   'TASK_ADD',
//   'TaskController',
// );
