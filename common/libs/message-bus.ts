import { Logger } from '@nestjs/common';
import amqp from 'amqplib/callback_api';
const CONN_URL = process.env.RABBIT_CONN_URL;
let amqpConn = null;
let pubChannel = null;
const EXCHANGE_DEFAULT = process.env.EXCHANGE_DEFAULT || 'EXCHANGE_DEFAULT';
const QUEUERETRY = Number(process.env.QUEUERETRY || 5);
const PREFETCH = Number(process.env.PREFETCH || 1);
const ROUTINGKEY_DEFAULT = '';
const NODE_ENV = process.env.NODE_ENV || 'DEV';
import * as pack from '../../package.json';
const NAME_SERVICE = pack.name || 'project_base';

// start();
export class MessageBus {
  async sendToQueue(content, exchange, routingKey) {
    try {
      return await this.publish(
        { payload: content },
        (exchange = exchange || EXCHANGE_DEFAULT),
        routingKey,
      );
    } catch (error) {
      Logger.log(error);

      throw error;
    }
  }
  async deleteQueue(nameQueue) {
    pubChannel.deleteQueue(nameQueue);
    return true;
  }
  async assertQueueExpirePauseTime(routingKey, msg, ttl) {
    try {
      return new Promise((resolve, reject) => {
        amqp.connect(CONN_URL, function (error0, connection) {
          if (error0) {
            reject(error0);
            throw error0;
          }
          connection.createChannel(function (error1, channel) {
            if (error1) {
              throw error1;
            }
            channel.assertQueue(routingKey, {
              durable: false,
              autoDelete: true,
              arguments: {
                'x-dead-letter-exchange': EXCHANGE_DEFAULT,
                'x-dead-letter-routing-key': 'QUEUE_EXPIRE_PAUSE_SUBCRIBER',
              },
            });
            channel.sendToQueue(
              routingKey,
              Buffer.from(JSON.stringify({ payload: msg })),
              {
                persistent: true,
                expiration: ttl,
              },
            );
            resolve('ok');
          });
          setTimeout(function () {
            connection.close();
          }, 500);
        });
      });
    } catch (error) {
      console.log(error);
    }
  }
  async assertQueueExpireStopTime(routingKey, msg, ttl) {
    try {
      return new Promise((resolve, reject) => {
        amqp.connect(CONN_URL, function (error0, connection) {
          if (error0) {
            reject(error0);
            throw error0;
          }
          connection.createChannel(function (error1, channel) {
            if (error1) {
              throw error1;
            }
            channel.assertQueue(routingKey, {
              durable: false,
              autoDelete: true,
              arguments: {
                'x-dead-letter-exchange': EXCHANGE_DEFAULT,
                'x-dead-letter-routing-key': 'QUEUE_EXPIRE_STOP_SUBCRIBER',
              },
            });
            channel.sendToQueue(
              routingKey,
              Buffer.from(JSON.stringify({ payload: msg })),
              {
                persistent: true,
                expiration: ttl,
              },
            );
            resolve('ok');
          });
          setTimeout(function () {
            connection.close();
          }, 500);
        });
      });
    } catch (error) {
      console.log(error);
    }
  }
  async sendToQueueExpireTime(content, exchange, routingKey, ttl) {
    try {
      return await this.publishExpireTime(
        { payload: content },
        (exchange = exchange || EXCHANGE_DEFAULT),
        routingKey,
        ttl,
      );
    } catch (error) {
      Logger.log(error);

      throw error;
    }
  }

  /**
   * build tên queue
   * @param name tên queue
   * @param nameClass tên class
   * @param nameDomain tên service
   * @returns string
   */
  buildNameQueue(name, nameClass?, nameDomain = NAME_SERVICE) {
    return `queue_${NODE_ENV}_${nameDomain}_${nameClass}_${name}`.toUpperCase();
  }

  /**
   * Hàm phục vụ việc bắn dữ liệu vào queue
   * @param {Object} content dữ liệu đẩy vào queue
   * @param {String} exchange tên exchange
   * @param {String} routingKey tên routingKey
   * @returns
   */
  async publish(content, exchange, routingKey) {
    try {
      return await pubChannel_(
        exchange || EXCHANGE_DEFAULT,
        routingKey || ROUTINGKEY_DEFAULT,
        Buffer.from(JSON.stringify(content)),
      );
    } catch (err) {
      throw err;
    }
  }
  async publishExpireTime(content, exchange, routingKey, ttl) {
    try {
      return await pubChannelExpireTime_(
        exchange || EXCHANGE_DEFAULT,
        routingKey || ROUTINGKEY_DEFAULT,
        Buffer.from(JSON.stringify(content)),
        ttl,
      );
    } catch (err) {
      throw err;
    }
  }
  /**
   * Khởi tạo worker
   * @param {controller} controller class
   * @param {string} nameFunc name func
   * @param {string} nameQueue name queue
   * @param {interceptor} interceptor interceptor
   */
  async run(controller, nameFunc, nameQueue, interceptor?) {
    if (pubChannel == null) {
      await new Promise((resolve) => setTimeout(resolve, 1000)); // tránh max CPU
      this.run(controller, nameFunc, nameQueue, interceptor || null);
    } else {
      //Cấu hình exchange and bind queue to exchange
      pubChannel.assertExchange(EXCHANGE_DEFAULT, 'direct');
      pubChannel.prefetch(PREFETCH);

      pubChannel.assertQueue(nameQueue, {
        arguments: {
          'x-dead-letter-exchange': EXCHANGE_DEFAULT,
          'x-dead-letter-routing-key': nameQueue + '_RETRY',
        },
      });
      pubChannel.assertQueue(nameQueue + '_RETRY', {
        arguments: {
          'x-message-ttl': 5000,
          'x-dead-letter-exchange': EXCHANGE_DEFAULT,
          'x-dead-letter-routing-key': nameQueue,
        },
      });
      pubChannel.assertQueue(nameQueue + '_ERROR');

      pubChannel.bindQueue(nameQueue, EXCHANGE_DEFAULT, nameQueue);
      pubChannel.bindQueue(
        nameQueue + '_RETRY',
        EXCHANGE_DEFAULT,
        nameQueue + '_RETRY',
      );
      pubChannel.bindQueue(
        nameQueue + '_ERROR',
        EXCHANGE_DEFAULT,
        nameQueue + '_ERROR',
      );
      //
      pubChannel.consume(
        nameQueue,
        async (msg) => {
          try {
            const input = JSON.parse(msg.content.toString());
            let payload = input.payload;
            if (!payload) throw Logger.log('payload empty');
            let rs = null;
            if (interceptor) {
              payload = interceptor(payload, nameFunc);
              rs = await controller[nameFunc](payload);
            } else {
              rs = await controller[nameFunc](payload);
            }

            Logger.log(rs);
            pubChannel.ack(msg);
          } catch (error) {
            // xử lý retries
            let retry = { count: 0 };
            if (msg.properties.headers['x-death']) {
              retry = msg.properties.headers['x-death'][0];
            } else {
              retry.count = 0;
            }
            if (retry.count < QUEUERETRY) {
              pubChannel.nack(msg, false, false);
            } else {
              // chuyển sang queue chờ xử lý bằng tay
              this.publish(
                msg.content.toString(),
                EXCHANGE_DEFAULT,
                nameQueue + '_ERROR',
              );
              pubChannel.ack(msg);
              Logger.error('send queue error');
            }
            Logger.error(retry.count);
            Logger.error(error);
            Logger.error(msg.content.toString());
          }
        },
        { noAck: false },
      );
    }
  }
  async runExpireTTL(controller, nameFunc, nameQueue, interceptor?) {
    if (pubChannel == null) {
      await new Promise((resolve) => setTimeout(resolve, 1000)); // tránh max CPU
      this.run(controller, nameFunc, nameQueue, interceptor || null);
    } else {
      //Cấu hình exchange and bind queue to exchange
      pubChannel.assertExchange(EXCHANGE_DEFAULT, 'direct');
      pubChannel.prefetch(PREFETCH);
      pubChannel.assertQueue(nameQueue, {
        arguments: {
          'x-dead-letter-exchange': EXCHANGE_DEFAULT,
          'x-dead-letter-routing-key': nameQueue + '_RETRY',
        },
      });
      pubChannel.assertQueue(nameQueue + '_RETRY', {
        arguments: {
          'x-message-ttl': 5000,
          'x-dead-letter-exchange': EXCHANGE_DEFAULT,
          'x-dead-letter-routing-key': nameQueue,
        },
      });
      pubChannel.assertQueue(nameQueue + '_EXPIRE_WAIT', {
        arguments: {
          'x-dead-letter-exchange': EXCHANGE_DEFAULT,
          'x-dead-letter-routing-key': nameQueue,
        },
      });
      pubChannel.assertQueue(nameQueue + '_ERROR');

      pubChannel.bindQueue(nameQueue, EXCHANGE_DEFAULT, nameQueue);
      pubChannel.bindQueue(
        nameQueue + '_EXPIRE_WAIT',
        EXCHANGE_DEFAULT,
        nameQueue + '_EXPIRE_WAIT',
      );
      pubChannel.bindQueue(
        nameQueue + '_RETRY',
        EXCHANGE_DEFAULT,
        nameQueue + '_RETRY',
      );
      pubChannel.bindQueue(
        nameQueue + '_ERROR',
        EXCHANGE_DEFAULT,
        nameQueue + '_ERROR',
      );
      //
      pubChannel.consume(
        nameQueue,
        async (msg) => {
          try {
            const input = JSON.parse(msg.content.toString());
            let payload = input.payload;
            if (!payload) throw Logger.log('payload empty');
            let rs = null;
            if (interceptor) {
              payload = interceptor(payload, nameFunc);
              rs = await controller[nameFunc](payload);
            } else {
              rs = await controller[nameFunc](payload);
            }

            Logger.log(rs);
            pubChannel.ack(msg);
          } catch (error) {
            // xử lý retries
            let retry = { count: 0 };
            if (msg.properties.headers['x-death']) {
              retry = msg.properties.headers['x-death'][0];
            } else {
              retry.count = 0;
            }
            if (retry.count < QUEUERETRY) {
              pubChannel.nack(msg, false, false);
            } else {
              // chuyển sang queue chờ xử lý bằng tay
              this.publish(
                msg.content.toString(),
                EXCHANGE_DEFAULT,
                nameQueue + '_ERROR',
              );
              pubChannel.ack(msg);
              Logger.error('send queue error');
            }
            Logger.error(retry.count);
            Logger.error(error);
            Logger.error(msg.content.toString());
          }
        },
        { noAck: false },
      );
    }
  }
  /**
   *  Khoi tao worker custom
   * @param {string} nameQueue
   * @param {string} nameExchange
   * @param {string} typeExchange [direct, fanout, topic] default = direct
   * @param {string} routingKey
   * @param {number} prefetch
   * @returns
   */
  async runCustom(
    controller,
    nameFunc,
    nameQueue,
    nameExchange?,
    typeExchange?,
    routingKey?,
    prefetch = 1,
  ) {
    if (pubChannel == null) {
      await new Promise((resolve) => setTimeout(resolve, 1000)); // tránh max CPU
      this.runCustom(
        controller,
        nameFunc,
        nameQueue,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      );
    } else {
      //Cấu hình exchange and bind queue to exchange
      pubChannel.assertExchange(nameExchange, typeExchange || 'direct');
      pubChannel.prefetch(prefetch);

      pubChannel.assertQueue(nameQueue);
      pubChannel.bindQueue(nameQueue, nameExchange, routingKey);

      pubChannel.consume(
        nameQueue,
        async (msg) => {
          try {
            const input = JSON.parse(msg.content.toString());

            if (!input) throw Logger.log('input empty');
            let rs = null;
            rs = await controller[nameFunc](input);

            Logger.log(rs);
            pubChannel.ack(msg);
          } catch (error) {
            pubChannel.nack(msg, false, false);
            Logger.error(error);
            Logger.error(msg.content.toString());
          }
        },
        { noAck: false },
      );
    }
  }

  /**
   * Khởi tạo worker
   * @param {controller} controller class
   * @param {string} nameFunc name func
   * @param {string} nameQueue name queue
   * @param {interceptor} interceptor interceptor
   */
  async runCustomProvider(
    controller,
    nameFunc,
    nameQueue,
    nameExchange?,
    typeExchange?,
    routingKey?,
    prefetch = 1,
  ) {
    if (pubChannel == null) {
      await new Promise((resolve) => setTimeout(resolve, 1000)); // tránh max CPU
      this.runCustomProvider(
        controller,
        nameFunc,
        nameQueue,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      );
    } else {
      //Cấu hình exchange and bind queue to exchange
      pubChannel.assertExchange(nameExchange, 'direct');
      pubChannel.prefetch(PREFETCH);

      pubChannel.assertQueue(nameQueue, {
        arguments: {
          'x-dead-letter-exchange': nameExchange,
          'x-dead-letter-routing-key': nameQueue + '_RETRY',
        },
      });
      pubChannel.assertQueue(nameQueue + '_RETRY', {
        arguments: {
          'x-message-ttl': 5000,
          'x-dead-letter-exchange': nameExchange,
          'x-dead-letter-routing-key': nameQueue,
        },
      });
      pubChannel.assertQueue(nameQueue + '_ERROR');

      pubChannel.bindQueue(nameQueue, nameExchange, nameQueue);
      pubChannel.bindQueue(
        nameQueue + '_RETRY',
        nameExchange,
        nameQueue + '_RETRY',
      );
      pubChannel.bindQueue(
        nameQueue + '_ERROR',
        nameExchange,
        nameQueue + '_ERROR',
      );
      //
      pubChannel.consume(
        nameQueue,
        async (msg) => {
          try {
            const input = JSON.parse(msg.content.toString());
            let payload = input.payload;
            if (!payload) throw Logger.log('payload empty');
            let rs = null;

            rs = await controller[nameFunc](payload);

            Logger.log(rs);
            pubChannel.ack(msg);
          } catch (error) {
            // xử lý retries
            let retry = { count: 0 };
            if (msg.properties.headers['x-death']) {
              retry = msg.properties.headers['x-death'][0];
            } else {
              retry.count = 0;
            }
            if (retry.count < QUEUERETRY) {
              pubChannel.nack(msg, false, false);
            } else {
              // chuyển sang queue chờ xử lý bằng tay
              this.publish(
                msg.content.toString(),
                nameExchange,
                nameQueue + '_ERROR',
              );
              pubChannel.ack(msg);
              Logger.error('send queue error');
            }
            Logger.error(retry.count);
            Logger.error(error);
            Logger.error(msg.content.toString());
          }
        },
        { noAck: false },
      );
    }
  }
}

// hàm để khởi tạo kết nói đến queue
function start() {
  amqp.connect(CONN_URL + '?heartbeat=60', function (err, conn) {
    if (err) {
      Logger.error('[AMQP]', err.message);
      return setTimeout(start, 1000);
    }
    conn.on('error', function (err) {
      if (err.message !== 'Connection closing') {
        Logger.error('[AMQP] conn error', err.message);
      }
    });
    conn.on('close', function () {
      Logger.error('[AMQP] reconnecting');
      return setTimeout(start, 1000);
    });
    Logger.log('[AMQP] connected');
    amqpConn = conn;
    startPublisher();
  });
}

// mở một kênh kết nối đến queue.
function startPublisher() {
  try {
    amqpConn.createConfirmChannel(function (err, ch) {
      // kênh kết nối có xác nhận
      if (closeOnErr(err)) return;

      ch.on('error', function (err) {
        Logger.error('[AMQP] channel error', err.message);
      });
      ch.on('close', function () {
        Logger.log('[AMQP] channel closed');
        startPublisher();
      });
      pubChannel = ch;
    });
  } catch (error) {
    Logger.log(error);
  }
}

function pubChannel_(exchange, routingKey, content) {
  return new Promise((resolve, reject) => {
    try {
      pubChannel.publish(
        exchange,
        routingKey,
        content,
        { persistent: true },
        function (err, ok) {
          if (err) {
            Logger.error('[AMQP] publish', err);
            reject(err);
          } else {
            resolve(true);
          }
        },
      );
    } catch (err) {
      Logger.error('[AMQP] publish', err.message);
      reject(err);
    }
  });
}
function pubChannelExpireTime_(exchange, routingKey, content, ttl) {
  return new Promise((resolve, reject) => {
    try {
      pubChannel.publish(
        exchange,
        routingKey,
        content,
        { persistent: true, expiration: ttl },
        function (err, ok) {
          if (err) {
            Logger.error('[AMQP] publish', err);
            reject(err);
          } else {
            resolve(true);
          }
        },
      );
    } catch (err) {
      Logger.error('[AMQP] publish', err.message);
      reject(err);
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  Logger.error('[AMQP] error', err);
  amqpConn.close();
  return true;
}
