import axios from 'axios';
import { config as CONFIG } from './app.config';
import { Logger } from '@nestjs/common';

export class Request {
  private instance;
  private config;
  /**
   *
   * @param {string} _nameService
   * @param {object} _config config tùy biến
   */
  constructor(_nameService, _config?) {
    this.config = _config || CONFIG[_nameService];
    this.instance = axios.create(this.config.config);
  }

  /**
   *
   * @param {string}  _module  tên module muốn call đến
   * @param {string} _function tên của hàm muốn call
   * @param {object} _params params truyền vào của phương thức GetList
   * @param {object} _body body của request. sử dụng cho POST, PUT
   * @param {object} _pathVariables path của request. sử dụng cho PUT, Delete, GetById
   * @param {object} _headers
   * @param {object} _config cấu hình config tùy chỉnh
   * @returns
   */
  async requestHTTP(
    _module,
    _function,
    _params = {},
    _body = {},
    _pathVariables = {},
    _headers = {},
    _config = this.config,
  ) {
    try {
      const config = {
        method: _config.module[_module].function[_function].method,
        url: `${_config.module[_module].config.baseURL}${_config.module[
          _module
        ].function[_function].url(_pathVariables)}`,
        data: _body,
        params: _params,
        headers: {
          Authorization: `basic ${process.env.PUBLIC_KEY}`,
          ..._headers,
        },
      };

      const result = await this.instance(config);
      return result.data;
    } catch (error) {
      Logger.log('begin error requestHTTP');
      Logger.log(_module);
      Logger.log(_function);
      Logger.log(_params);
      Logger.log(_body);
      Logger.log(_pathVariables);
      Logger.log(_config);
      Logger.log(error);
      Logger.log('end error requestHTTP');
      throw error;
    }
  }

  // async requestQueue() {}
}

/* ví dụ
 import { Request } from '../../../common/libs/request';
 const request = new Request('product-service');
    return await request.requestHTTP(
      'addon',
      'edit',
      {},
      { updateBy: 'aaaa' },
      { id: 'fc26e9a0-d552-4aee-a70e-7b923ba7aa26' },
    );
*/
