import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as firebase from 'firebase-admin';
import { DecodedIdToken } from 'firebase-admin/lib/auth/token-verifier';
@Injectable()
export class AuthMiddleware implements NestMiddleware {
  async use(req: Request, res: Response, next: NextFunction) {
    if (
      req.headers['x-api-key'] &&
      req.headers['x-api-key'] === process.env.PRIVATE_TOKEN
    ) {
      next();
      return;
    }
    if (!req.headers.authorization) {
      throw new HttpException('Unauthen', HttpStatus.BAD_REQUEST);
    }
    const token = req.headers.authorization.split(' ')[1];
    const firebaseUser: DecodedIdToken = await firebase
      .auth()
      .verifyIdToken(token)
      .catch(() => {
        return null;
      });
    if (!firebaseUser) {
      throw new HttpException(
        'Invalid firebase idToken',
        HttpStatus.BAD_REQUEST,
      );
    }
    req['firebaseUser'] = firebaseUser;
    next();
  }
}
