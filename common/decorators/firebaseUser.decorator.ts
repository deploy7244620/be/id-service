import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { DecodedIdToken } from 'firebase-admin/lib/auth/token-verifier';
export const FirebaseUser = createParamDecorator<DecodedIdToken>(
  (_, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request['firebaseUser'];
  },
);
