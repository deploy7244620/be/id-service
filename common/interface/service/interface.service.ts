export interface IService {
  _repo;

  add(input: any);
  update(id: any, data: any);
  delete(id: any);
  findById(id: any);
  findOne(option: any);
  select(input: any);
  search(input);
}
