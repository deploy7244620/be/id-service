import { HttpException, HttpStatus } from '@nestjs/common';
import { MessageBus } from 'common/libs/message-bus';
import { IBaseRepository, OutputSelect } from './interface.repository';
const messageBus = new MessageBus();
const EXCHANGE_DEFAULT = process.env.EXCHANGE_DEFAULT || 'EXCHANGE_DEFAULT'
const NAME_QUEUE = {
  INSERT: {
    nameQueue: 'INSERT',
    nameWorker: 'insertDataElasticsearch',
  },
  UPDATE: {
    nameQueue: 'UPDATE',
    nameWorker: 'updateDataElasticsearch',
  },
  DELETE: {
    nameQueue: 'DELETE',
    nameWorker: 'deleteDataElasticsearch',
  },
};

export abstract class AProxy<Entity> implements IBaseRepository<Entity> {
  constructor(public nameModule, public storage, public storageSearch?) {
    if (this.storageSearch) {
      for (const key in NAME_QUEUE) {
        if (Object.prototype.hasOwnProperty.call(NAME_QUEUE, key)) {
          const element = NAME_QUEUE[key];
          messageBus.run(
            this,
            element.nameWorker,
            messageBus.buildNameQueue(element.nameQueue, this.nameModule),
          );
        }
      }
    }
  }

  private async insertDataElasticsearch(input) {
    return await this.storageSearch.createDoc(
      this.storageSearch.nameIndex,
      input,
    );
  }

  private async updateDataElasticsearch(input) {
    return await this.storageSearch.updateDoc(
      this.storageSearch.nameIndex,
      input.id,
      input.data,
    );
  }

  private async deleteDataElasticsearch(input) {
    return await this.storageSearch.deleteDoc(
      this.storageSearch.nameIndex,
      input,
    );
  }

  async findOne(option: any): Promise<Entity> {
    try {
      return await this.storage.findOne(option);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async insert(input: Entity): Promise<Entity> {
    try {
      const data = await this.storage.insert(input);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          input,
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.INSERT.nameQueue,
            this.nameModule,
          ),
        );
      }
      return data;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async update(id: string, data: any): Promise<Entity> {
    try {
      const rs = await this.storage.update(id, data);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          { id, data },
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.UPDATE.nameQueue,
            this.nameModule,
          ),
        );
      }
      return rs;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async delete(input: string): Promise<any> {
    try {
      const rs = await this.storage.delete(input);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          input,
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.DELETE.nameQueue,
            this.nameModule,
          ),
        );
      }
      return rs;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async select(input: any): Promise<OutputSelect<Entity>> {
    try {
      return await this.storage.select(input);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async findById(id: any): Promise<Entity> {
    try {
      return await this.storage.findById(id);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async search(input) {
    try {
      return await this.storageSearch.search(
        this.storageSearch.nameIndex,
        input,
      );
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
}
