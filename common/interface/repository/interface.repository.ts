export class OutputSelect<Entity> {
  constructor(public data: Entity[]) {}
  page: number;
  size: number;
  totalPage: number;
  totalCount: number;
  count: number;
}
export interface IBaseRepository<Entity> {
  insert(input: Entity): Promise<Entity>;
  update(id: string, data: any): Promise<Entity>;
  delete(input: string): Promise<any>;
  select(input: any): Promise<OutputSelect<Entity>>;
  findById(id): Promise<Entity>;
  findOne(option): Promise<Entity>;
}
