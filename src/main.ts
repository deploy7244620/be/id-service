import * as dotenv from 'dotenv';
// dotenv.config();
dotenv.config({ path: `${process.cwd()}/env/${process.env.NODE_ENV}.env` });
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import * as pack from '../package.json';
import * as LogstashTransport from 'winston-logstash-transport';
import { HttpExceptionFilter } from 'common/decorators/http-exception-filters';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as firebase from 'firebase-admin';
const LOGSTASH_HOST = process.env.LOGSTASH_HOST || '119.82.135.154';
const LOGSTASH_PORT = process.env.LOGSTASH_PORT || 31275;
const NODE_ENV = process.env.NODE_ENV || 'development';

console.log(process.env);

async function bootstrap() {
  const app = await NestFactory.create(
    AppModule,
    NODE_ENV == 'production'
      ? {
          logger: WinstonModule.createLogger({
            format: winston.format.combine(
              winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss',
              }),
              winston.format.json(),
            ),
            defaultMeta: {
              service: pack.name,
              tags: [process.env.NODE_ENV],
              env: process.env.NODE_ENV,
            },
            transports: [
              new winston.transports.Console({}),
              new winston.transports.File({ filename: 'logs/logger.log' }),
              new LogstashTransport.LogstashTransport({
                host: LOGSTASH_HOST,
                port: LOGSTASH_PORT,
              }),
            ],
            // other options
          }),
        }
      : {},
  );
  app.enableCors();

  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));
  app.useGlobalFilters(new HttpExceptionFilter());
  firebase.initializeApp({
    credential: firebase.credential.cert({
      clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
      privateKey: process.env.FIREBASE_PRIVATE_KEY
        ? process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/gm, '\n')
        : undefined,
      projectId: process.env.FIREBASE_PROJECTID,
    } as Partial<firebase.ServiceAccount>),
  });

  const config = new DocumentBuilder()
    .setTitle(`${pack.name} API`)
    .setDescription(`${pack.name} API`)
    .setVersion('1.0')
    .addServer(`/api/v1/${pack.name}`)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    ignoreGlobalPrefix: true,
  });
  SwaggerModule.setup(`/api/v1/${pack.name}/swagger`, app, document);
  await app.listen(3000);
}
bootstrap();
