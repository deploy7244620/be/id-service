import { IBaseRepository } from 'common/interface/repository/interface.repository';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IRepository<T> extends IBaseRepository<T> {}
