import { Inject, Injectable } from '@nestjs/common';
import { IRepository } from './repository/interface.repository';
import { IEntity } from './entity';
import { AService } from 'common/interface/service/abstract.service';
import { RepositoryProxy } from './repository/repository.proxy';
import { Subscribe } from 'common/decorators/custom-decorators';
// import { RepositoryElasticSearch } from './repository/repository.elasticsearch';
const rootFolder = __dirname.split('/').pop();

@Injectable()
export class Service extends AService {
  constructor(
    @Inject('repoMongodb')
    private repositoryMongo: IRepository<IEntity>,
  ) {
    super(new RepositoryProxy<IEntity>(rootFolder, repositoryMongo));
  }
}
