import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  userId: string;
  age: number;
  gender: string;
  weight: number;
  height: number;
  targetNutrition: string; //mục tiêu dịch dưỡng: giảm cân, tăng cần,...
  healthHistory: string; //lịch sử bệnh tật
  nutritionalConstraints: string; //ràng buộc ăn uống, dị ứng món ăn,...
}

@Entity({ name: 'user-health-profile' })
@Index(['userId'], { unique: true })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  userId: string;

  @Column({ nullable: true })
  age: number;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  weight: number;

  @Column({ nullable: true })
  height: number;

  @Column({ nullable: true })
  targetNutrition: string; //mục tiêu dịch dưỡng: giảm cân, tăng cần,...

  @Column({ nullable: true })
  healthHistory: string; //lịch sử bệnh tật

  @Column({ nullable: true })
  nutritionalConstraints: string; //ràng buộc ăn uống, dị ứng món ăn,...

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
