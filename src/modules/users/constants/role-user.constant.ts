export const RoleUser = {
  ADMIN: 'admin',
  GUEST: 'guest',
  RESTAURANT: 'restaurant',
};
