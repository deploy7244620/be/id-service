import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsDate,
  IsDateString,
  IsEmpty,
  IsEnum,
  IsNotEmpty,
  isNumber,
  IsNumber,
  isObject,
  IsObject,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import { IEntity } from '../entity';
import { RoleUser } from '../constants/role-user.constant';

export class CreateDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  password: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  fullName: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  firstName: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  lastName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ nullable: true })
  phone: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ nullable: true })
  email: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  city: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  district: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  wards: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  address: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  postalCode: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  photoUrl: string;

  @IsNotEmpty()
  @IsString()
  @IsEnum(Object.values(RoleUser))
  @ApiProperty({ nullable: true })
  role: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
