import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { EditDto } from './dto/edit.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
import { AddUserDto } from './dto/add-user.dto';
import { FirebaseUser } from 'common/decorators/firebaseUser.decorator';
import { DecodedIdToken } from 'firebase-admin/lib/auth/token-verifier';
const rootFolder = __dirname.split('/').pop();
@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}

  @Get('/me')
  async getMe(@FirebaseUser() firebaseUser: DecodedIdToken) {
    return await this._service.findOne({ firId: firebaseUser.uid });
  }

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post('/sign-up')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  async add(@Body() body: CreateDto) {
    return await this._service.add(body);
  }

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  create(@Body() body: AddUserDto) {
    return this._service.addUser(body);
  }

  @Delete('/:id')
  async delete(@Param('id') p_id) {
    return await this._service.delete(p_id);
  }

  @Get()
  async getList(@Query() input: GetListDto) {
    return await this._service.select(input);
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    return await this._service.findById(p_id);
  }

  @Put('/:id')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  async edit(@Param('id') p_id, @Body() body: EditDto) {
    return await this._service.update(p_id, body);
  }
}
