import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  fullName: string;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  city: string;
  district: string;
  wards: string;
  address: string;
  postalCode: string;
  photoUrl: string;
  firId: string; //id luu tren firebase
}

@Entity({ name: 'users' })
@Index(['phone'], { unique: true })
@Index(['email'], { unique: true })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  fullName: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  district: string;

  @Column({ nullable: true })
  wards: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  postalCode: string;

  @Column({ nullable: true })
  photoUrl: string;

  @Column({ nullable: true })
  firId: string; //id luu tren firebase

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
