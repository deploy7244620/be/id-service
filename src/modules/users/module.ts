import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityMongodb } from './entity';
import { RepositoryMongodb } from './repository/repository.mongodb';
import { ModuleController } from './controller';
import { Service } from './service';
import { AuthMiddleware } from 'common/middleware/authentication';
const rootFolder = __dirname.split('/').pop();
@Module({
  imports: [TypeOrmModule.forFeature([EntityMongodb], 'mongodb')],
  controllers: [ModuleController],
  providers: [
    Service,
    {
      provide: 'repoMongodb',
      useClass: RepositoryMongodb,
    },
  ],
  exports: [Service],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude({
        path: `${rootFolder.toLocaleLowerCase()}/sign-up`,
        method: RequestMethod.POST,
      })
      .forRoutes(ModuleController);
  }
}
