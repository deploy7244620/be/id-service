import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { IRepository } from './repository/interface.repository';
import { IEntity } from './entity';
import { AService } from 'common/interface/service/abstract.service';
import { RepositoryProxy } from './repository/repository.proxy';
import { CreateDto } from './dto/create.dto';
import * as firebase from 'firebase-admin';
import { AddUserDto } from './dto/add-user.dto';
// import { RepositoryElasticSearch } from './repository/repository.elasticsearch';
const rootFolder = __dirname.split('/').pop();

@Injectable()
export class Service extends AService {
  constructor(
    @Inject('repoMongodb')
    private repositoryMongo: IRepository<IEntity>,
  ) {
    super(new RepositoryProxy<IEntity>(rootFolder, repositoryMongo));
  }

  async add(body: CreateDto) {
    try {
      const { email, phone, password } = body;
      const [userEmailExist, userPhoneExist] = await Promise.all([
        this._repo.findOne({ email }),
        this._repo.findOne({ phone }),
      ]);

      if (userEmailExist) {
        throw new HttpException('Email đã tồn tại', HttpStatus.BAD_REQUEST);
      }

      if (userPhoneExist) {
        throw new HttpException(
          'Số điện thoại đã tồn tại',
          HttpStatus.BAD_REQUEST,
        );
      }

      const result = await firebase
        .auth()
        .createUser({
          password: password,
          email: email,
        })
        .then((_userRecord) => {
          return _userRecord;
        })
        .catch((error) => {
          return error;
        });
      if (result.uid) {
        return await this._repo.insert({ ...body, firId: result.uid });
      } else {
        throw new HttpException(result, HttpStatus.BAD_REQUEST);
      }
    } catch (error) {
      throw error;
    }
  }

  async addUser(body: AddUserDto) {
    const { firebaseUser } = body;
    const existingUser = await this._repo.findOne({ firId: firebaseUser.uid });
    //check tai khoan ton tai chua
    if (existingUser) return existingUser;
    else {
      return this._repo.insert({ ...body, firId: firebaseUser.uid });
    }
  }
}
