import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscoveryDecoratorModule } from 'common/modules/discovery-decorator/discovery-decorator';
import { HealthModule } from 'common/modules/health/module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AddDefaultInterceptor } from 'common/interceptor/interceptor.add-default';
import { UserModule } from './modules/users/module';
import { UserRestaurantModule } from './modules/user-restaurant/module';
import { UserHealthProfileModule } from './modules/user-health-profile/module';
import { UserFollowRestaurantModule } from './modules/user-follow-restaurant/module';
import { UserFavouriteModule } from './modules/user-favourite/module';

@Module({
  imports: [
    UserModule,
    UserRestaurantModule,
    UserHealthProfileModule,
    UserFollowRestaurantModule,
    UserFavouriteModule,
    HealthModule,
    DiscoveryDecoratorModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      name: 'mongodb',
      url: process.env.MONGODB_DATABASE,
      entities: [],
      synchronize: true,
      autoLoadEntities: true,
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: AddDefaultInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .exclude({ path: '/health', method: RequestMethod.ALL })
      .forRoutes('*');
  }
}
