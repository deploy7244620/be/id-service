setup rabbitmq:
docker run -d -p 5672:5672 -p 15672:15672 --name some-rabbit -e RABBITMQ_DEFAULT_USER=userr -e RABBITMQ_DEFAULT_PASS=passwordd rabbitmq:3-management

setup redis:
docker pull redis:5.0.14-alpine

git hook
Tạo một file commit-msg trong thư mục .git/hooks với nội dung
#!/bin/sh
message=$(cat $1)
pattern="^(feat|fix|docs|style|refactor|test|chore)\([^\)]+\): .+$"
`
if ! echo "$message" | grep -qE "$pattern"; then
echo "Commit message format is invalid."
exit 1
fi

đặt quyền thực thi cho script này:

chmod +x .git/hooks/commit-msg
